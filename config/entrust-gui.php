<?php

return [
    'layout' => 'spark::layouts.app',
    'route-prefix' => 'corrections',
    'pagination' => [
        'users' => 10,
        'roles' => 10,
        'permissions' => 20,
    ],
    'middleware' => 'entrust-gui.admin',
    'middleware-role' => 'saycomputer-admin',
    'confirmable' => false,
];
