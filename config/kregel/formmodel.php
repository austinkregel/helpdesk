<?php

return [
  'using' => [
    'csrf' => true,
    'framework' => 'materialize-custom',
    'custom-framework' => function () {
      return new App\Packages\FormModel\MaterializeFileSpecial();
    },
  ],
];
