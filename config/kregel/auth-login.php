<?php

return [
    'base-layout' => 'spark::layouts.app',
    'prefix' => 'auth',
    'profile' => [
        'enabled' => true,
        'route' => 'profile',
    ],
];
