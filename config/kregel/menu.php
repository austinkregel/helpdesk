<?php

return [
    'brand' => [
        'default' => '<i class="fa fa-film red-text text-lighten-2" style="font-size:2rem;"></i>Theater Admin',
        'name' => function () {
           return config('kregel.menu.brand.default');
        },
        'link' => '/',
    ],
    'theme' => 'navbar-default',
    'items' => [
        // This menu will be shown regardless of whether or not
        // the login system is enabled!

//        'Pricing' => [
//            'link' => '#',
//            'icon' => 'fa fa-money'
//        ],
//        'Like Things?' => [
//            'link' => '#',
//            'icon' => 'fa fa-line-chart'
//        ]
    ],
    // If enabled this will add a login/logout button to your nav menu
    'login' => [
        'enabled' => true,
        'sign-in' => [
            // This menu will only be shown if the login system is enabled!
            // And is  only shown when the user is not logged in!
            'Login' => [
                'link' => 'login',
                'icon' => 'fa fa-sign-in',
            ],
            'Register' => [
                'link' => 'register',
                'icon' => 'fa fa-user-plus',
            ],
        ],
        'sign-out' => [
            // This is the main menu item, the rest is in a drop down.
            'My Account' => [
                // Drop down menu item that uses named route and a closure.
                'Settings' => [
                    'link' => 'settings?tab=profile',
                    'icon' => 'fa fa-cog fa-fw ',
                ],
                'Security' => [
                    'link' => 'settings?tab=security',
                    'icon' => 'fa fa-lock fa-fw ',
                ],
                'Subscription' => [
                    'link' => 'settings?tab=subscription',
                    'icon' => 'fa fa-refresh fa-fw ',
                ],
                '',
                'Logout' => [
                    'link' => 'auth-login::logout',
                    'icon' => 'fa fa-btn fa-fw fa-sign-out',
                ],
            ],
            'Administration' => [
                'Tickets' => [
                    'link' => function () {
                        return config('kregel.dispatch.route');
                    },
                    'icon' => 'fa fa-ticket fa-fw ',
                ],
                'Warden' => [
                    'link' => function () {
                        return config('kregel.warden.route');
                    },
                    'icon' => 'fa fa-lock fa-fw ',
                ],
                'Permissions' => [
                    'link' => 'entrust-gui::users.index',
                ],
            ],
        ],
        // This is the settings for the user-nav
        'user-nav' => [
            // This will enable/ disable the user-nav menu, usefull if you don't want to
            'enabled' => false,
            // This is what the user-nav will appear under.
            'menu-warding' => 'Profile',

            'user-img' => function () {
                return 'http://www.gravatar.com/avatar/'.md5(Auth::user()->email).'?d='.urlencode(route('identicon::main', [md5(Auth::user()->email)]));
            },
            // These are the items above the buttons and below the dark part.
            'user-body' => [
                // Link => working pairs
                'Wording' => function () {
                    if (Route::has('auth-login::')) {
                        return route('auth-login::');
                    }

                    return '';
                },
                '/things' => function () {
                    return '';
                },
                '/thinsgs' => function () {
                    return '';
                },
            ],
            'footer-left' => [
                'link' => function () {
                    return 'profile/'.str_slug(Auth::user()->name);
                },
                'text' => 'Profile',
            ],
            'footer-right' => [
                // Link, Wording array. This package will only ever pull the first
                // Two elements, no matter what. The first element must be a link
                // And the second part must be the wording. (how you wod it is up to you.
                // Note: Closures are not supported here... (Sorry)
                'link' => function () {
                    if (Route::has('auth-login::logout')) {
                        return route('auth-login::logout');
                    }

                    return '';
                },
                'text' => 'Sign out',
            ],
        ],
    ],
    'custom-css-framework' => function ($menu) {
        /*
         * Please note that this is NOT the recommended way to implement,
         * this is just meant to be a boiler plate for you to copy and
         * paste to make your own framework based menu handler.
         */

        return new App\Packages\Menu\MaterializePjax();
    },
];
