var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('app.scss')
        .browserify('app.js')
        .scripts([
            '../../../node_modules/materialize-css/dist/js/materialize.js',
            '../../../node_modules/chart.js/Chart.min.js',
            'main_scripts.js',
            'datatables.bootstrap.js',
        ]);
});
