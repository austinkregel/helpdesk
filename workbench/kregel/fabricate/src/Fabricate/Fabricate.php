<?php

namespace Kregel\Fabricate;

class Fabricate
{
    /**
     * This variable will store the list of desired classes so we
     * will not have to constantly query the config.list file.
     *
     * @var
     */
    private $classList;

    /**
     * Retrieve the config list once on the creation of the class.
     */
    public function __construct()
    {
        $this->classList = config('model.list');
    }

    /**
     * This function will assume that the $name variable is
     * the name of the class that we wish to construct.
     *
     * @param string $name
     * @param array  $args
     */
    public function __call($name, $args)
    {
        // Query the list of known classes and set class equal
        // to the resulting string if something is found.
        $class = $this->classList[(strtolower($name))];
        // If nothing is found throw a new Exception
        $class_exists = class_exists($class);
        if ($class_exists === false) {
            // Probably throws an error saying that this class is not found.
            return \ErrorException("There is not a class named [$name] in the configuration file.");
        } elseif ($class_exists !== false) {
            // Check if the array of arguments is empty, if they are
            // Then just return a new model of the queried class.
            if ($this->aempty($args)) {
                return new $class();
            }
            // Loop through the arguments
            foreach ($args as $arg) {
                // Construct a new Model object (what ever it may be)
                $model = new $class();
                // Fill that new model with the arguments, assuming it IS an array.
                // TODO: Should probably check to make sure that $arg is an array
                $model->fill($args);
                // Get the fillable info, if it contains a password bcrypt (hash/salt)
                // The password and set the current instance of the model's password
                // to the hash.
                foreach ($model->getFillable() as $field) {
                    if (stripos('password', $field) !== false | stripos('passwd', $field) !== false) {
                        $model->$field = bcrypt($model->$field);
                    }
                }
                // Possibly a more elegant way of doing the above?
                if (!empty($model->password)) {
                    $model->password = bcrypt($model->password);
                }
                // Save the model.
                $model->save();
            }

            return $model;
        }
    }

    private function aempty($searchingFor)
    {
        $Result = true;
        if (is_array($searchingFor) && count($searchingFor) > 0) {
            foreach ($searchingFor as $Value) {
                $Result = !($Result && $this->aempty($Value));
            }
        } else {
            $Result = empty($searchingFor);
        }

        return $Result;
    }
}
