<?php


class RouteTest extends TestCase
{
    /** @test */
    public function visit_auth_page()
    {
        $this->visit('auth/login')
            ->see('forgot your password');// Since we can see forgot password it must be valid
        $this->assertResponseOk();
    }

    /** @test */
    public function visit_reset_page()
    {
        $this->visit('auth/email')
            ->see('send password reset link');
        $this->assertResponseOk();
    }

    /** @test */
    public function visit_register_page()
    {
        $this->visit('auth/register')
            ->see('e-mail address');
        $this->assertResponseOk();
    }

    /** @test */
    public function visit_corrections_page()
    {
        $this->visit('corrections')
            ->see('Not broken!');
        $this->assertResponseOk();
    }
}
