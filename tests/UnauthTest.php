<?php


class UnauthTest extends TestCase
{
    /** @test */
    public function visit_home_page()
    {
        $this->visit('/')
            ->see('Theater Admin');
        $this->assertResponseOk();
    }
}
