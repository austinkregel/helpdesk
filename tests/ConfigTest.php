<?php


class ConfigTest extends TestCase
{
    /** @test */
    public function is_warden_available()
    {
        $this->assertNotNull(config('kregel.warden'));
    }

    /** @test */
    public function is_corrections_available()
    {
        $this->assertNotNull(config('kregel.corrections'));
    }

    /** @test */
    public function is_auth_login_available()
    {
        $this->assertNotNull(config('kregel.auth-login'));
    }

    /** @test */
    public function is_formmodel_available()
    {
        $this->assertNotNull(config('kregel.formmodel'));
    }

    /** @test */
    public function is_identicon_available()
    {
        $this->assertNotNull(config('kregel.identicon'));
    }

    /** @test */
    public function is_menu_available()
    {
        $this->assertNotNull(config('kregel.menu'));
    }
}
