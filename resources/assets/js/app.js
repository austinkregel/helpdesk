/*
 |--------------------------------------------------------------------------
 | Laravel Spark - Creating Amazing Experiences.
 |--------------------------------------------------------------------------
 |
 | First, we will load all of the "core" dependencies for Spark which are
 | libraries such as Vue and jQuery. This also loads the Spark helpers
 | for things such as HTTP calls, forms, and form validation errors.
 |
 | Next, we will create the root Vue application for Spark. This'll start
 | the entire application and attach it to the DOM. Of course, you may
 | customize this script as you desire and load your own components.
 |
 */
Vue.config.debug = true
require('laravel-spark/core/bootstrap');

// define
var vue_comment = Vue.extend({
    props: [ 'response', 'action', 'body', 'ticket_id', 'user_id', '_token'],
    methods:{
        makeRequest: function (e) {
            var self = this;
            console.log(this.$data);
            e.preventDefault();
            this.request(e.target.action,
                (this.$data)
                , function(responseArea){
                    var response = $('#response');
                    response.removeClass (function (index, css) {
                        return (css.match (/\balert-.*\s/g) || []).join(' ');
                    }).addClass('alert-success');

                    self.body = '';
                }, function(responseArea){
                    var response = $('#response');
                    response.removeClass (function (index, css) {
                        return (css.match (/\balert-.*\s/g) || []).join(' ');
                    }).addClass('alert-warning');

                }, function(responseArea){
                    var response = $('#response');
                    response.removeClass (function (index, css) {
                        return (css.match (/\balert-.*\s/g) || []).join(' ');
                    }).addClass('alert-danger');
                });
        },
        close: function (e) {
            this.response = '';
        },
        request: function (url, data, success, fail, nochange) {
            var xmlhttp = new XMLHttpRequest(),
                self = this;   // new HttpRequest instance
            xmlhttp.open("post", url);

            xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            xmlhttp.setRequestHeader("X-Requested-With", 'XMLHttpRequest');
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    var response = JSON.parse(xmlhttp.responseText);
                    self.response = 'Comment posted successfully';
                    var respArea = document.getElementById('response');
                    if (!respArea.classList.contains('alert)')) {
                        respArea.className = 'alert ';
                    }
                    switch (response.code) {
                        case 200:
                        case 202:
                            success(respArea);
                            break;
                        case 205:
                            nochange(respArea);
                            break;
                        default:
                            fail(respArea);
                    }
                }
            };
            xmlhttp.send(JSON.stringify(data));
        }
    },
    template: '<div id ="response" v-show="response" class="alert">{{ response }}<div class="close" @click="close">&times;</div></div>\
    <form :action="action" method="POST"  class="make-comment" style="background:white" @submit.prevent="makeRequest">\
        <input type="text" name="fake_body" style="display:none;">\
        <input type="text" name="body" placeholder="Comment on this" v-model="body" class="form-control input-sm" autocomplete="off" style="box-shadow:0 2px 5px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);">\
        <input type="hidden" name="ticket_id" :value="ticket_id" v-model="ticket_id">\
        <input type="hidden" name="_token"    :value="_token"    v-model="_token">\
        <input type="hidden" name="user_id"   :value="user_id"   v-model="user_id">\
        <input type="hidden" name="_method"   value="POST"> \
        <input type="submit" style="position:absolute;top:-1000px; left:-10000px;">\
    </form>'
});
var vue_show_more = Vue.extend({
    props: ['data'],
    data: function() {
        return {
            show: false,
            count:0
        };
    },
    methods:{
        toggleShow:function(){
            this.show = !this.show;
        }
    },
    template: '<ul class="show-more" >\
        <li v-for="info in data" v-show="($index > 4 ?show:true)">{{info.name}}</li>\
        </ul>\
        <a href="#!" class="more-link" v-on:click.prevent="toggleShow" v-if="data.length > 5">{{ !show ? data.length - 5 + " more..." : "less..."}} </a>'
});
var vue_comments = Vue.extend({
    props: ['data'],
    data: function() {
        return {
            show: false,
            count:0
        };
    },
    methods:{

    },
    template:'<li class="card " style="border-radius:5px; ">\
        <span class="card-title collapsible-header>\
            {{ $comment->user->name }}\
            <div class="close">&times;</div>\
            <div style="width:calc(100% - 20px);margin-top:-15px;margin-left:-5px;">\
                <span class="badge customize">Created: date("M d, Y H:i", strtotime($comment->created_at)) $comment->id</span>\
            </div>\
        </span>\
        <div class="collapsible-body">\
            <p>{{$comment->body}}</p>\
        </div>\
    </li>\
    <li style="height:1rem;width:100%;"></li>'
});
Vue.component('show-more-list', vue_show_more);
Vue.component('ticket-make-comment', vue_comment);
Vue.component('ticket-comment', vue_comments);
var vue_instance = require('laravel-spark');
var vue_app_vm = new Vue(vue_instance);

Spark.instance = vue_app_vm;
