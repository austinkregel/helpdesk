var that = this.vm;
(function ($) {
    $(function () {
        $('.button-collapse').sideNav();
        $('.parallax').parallax();
        $('.dropdown-button').dropdown();
        $('.modal-trigger').leanModal({dismissible: true, opacity: .5, in_duration: 300, out_duration: 200});
        $('.scrollspy').scrollSpy();
        $('.collapsible').collapsible();
    });
})(jQuery);
var header = document.querySelector(".navbar-fixed")
window.addEventListener('scroll', function (e) {
    var distanceY = window.pageYOffset || document.documentElement.scrollTop,
        shrinkOn = 10;
    if (distanceY > shrinkOn) {
        if (!header.classList.contains('smaller'))
            header.className = 'smaller ' + header.className;

    } else {
        if (header.classList.contains("smaller")) {
            header.className = header.className.replace(/\bsmaller\b\s/g, '');
        }
    }
});



// does current browser support PJAX
//   if ($.support.pjax) {
//   $.pjax.defaults.timeout = 1000; // time in milliseconds
//   $(document).pjax('.p-link', '#px-wrapper');
//   $(document).on('pjax:start', function() { NProgress.start(); });
//   $(document).on('pjax:end',   function() { NProgress.done();  });
//   $(document).on('pjax:success', function(){
//         vue_app_vm = null;
//         vue_app_vm = new Vue(require('laravel-spark'));

//         $('.dropdown-button').dropdown();
//         NProgress.done();
//   });

//   console.log('PJax Loaded');
//   }
// require('materialize-css/js');


