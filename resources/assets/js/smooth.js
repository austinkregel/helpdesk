window.smoothScroll = require('smooth-scroll/dist/js/smooth-scroll');


setTimeout(function () {
    (function ($) {
        $(function () {
            $('.button-collapse').sideNav();
            $('.parallax').parallax();
            $('.dropdown-button').dropdown();
            $('.modal-trigger').leanModal({
                dismissible: true, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                in_duration: 300, // Transition in duration
                out_duration: 200, // Transition out duration
            });
            $('select').material_select();

            $('.scrollspy').scrollSpy();
        }); // end of document ready
        // does current browser support PJAX
//   if ($.support.pjax) {
//   $.pjax.defaults.timeout = 1000; // time in milliseconds
//   $(document).pjax('.p-link', '#px-wrapper');
//   $(document).on('pjax:start', function() { NProgress.start(); });
//   $(document).on('pjax:end',   function() { NProgress.done();  });
//   $(document).on('pjax:success', function(){
//         vue_app_vm = null;
//         vue_app_vm = new Vue(require('laravel-spark'));

//         $('.dropdown-button').dropdown();
//         NProgress.done(); 
//   });

//   console.log('PJax Loaded');
//   }

    })(jQuery); // end of jQuery name space

    var header = document.querySelector(".navbar-fixed")
    window.addEventListener('scroll', function (e) {
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 10;
        if(header != null)
            if (distanceY > shrinkOn) {
                if (!header.classList.contains('smaller'))
                    header.className = 'smaller ' + header.className;
    
            } else {
                if (header.classList.contains("smaller")) {
                    header.className = header.className.replace(/\bsmaller\b\s/g, '');
                }
            }
    });
}, 10);
// require('materialize-css');
/*!
 * Materialize v0.97.3 (http://materializecss.com)
 * Copyright 2014-2015 Materialize
 * MIT License (https://raw.githubusercontent.com/Dogfalo/materialize/master/LICENSE)
 */
