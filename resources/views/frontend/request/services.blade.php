<div class="panel panel-default panel-flush">
    <div class="panel-heading">
        Avialable Services
    </div>
    <div class="spark-panel-body panel-body">
        <div class="spark-settings-tabs">
            <ul class="nav-wrapper nav spark-settings-tabs-stacked" role="tablist">
                <?php
                $i = 2000;
                $dropdown = '';
                $roles = Auth::user()->roles;

                ?>
                @foreach($roles as $role)
                    @foreach($role->services as $service)
                    <!-- Settings Dropdown -->
                    <!-- Authenticated Right Dropdown -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-button" data-activates="side-bar-menu-{{ $i }}" >
                            {{ ucwords($service->name) }} <i class="material-icons right">arrow_drop_down</i>
                        </a>
                        <?php
                            $dropdown .= '<ul class="dropdown-content" id="side-bar-menu-'. $i .'">
                            <!-- Settings -->
                            <li>
                                <a href="' . route('frontend::service', [$service->id, str_slug($service->name)]) .'" class="p-link">
                                    <i class="fa fa-btn fa-fw fa-cog"></i>Request a ' .ucwords($service->name) .'
                                </a>
                            </li>
                            <li>
                                <a href="'.route('frontend::service', [$service->id, str_slug($service->name)]) .'" class="p-link">
                                    <i class="fa fa-btn fa-fw fa-cog"></i>View previous '.ucwords($service->name).'s
                                </a>
                            </li>
                        </ul>';
                            $i++
                        ?>
                    </li>
                    @endforeach
                @endforeach
            </ul>
            {!! $dropdown !!}
        </div>
    </div>
</div>