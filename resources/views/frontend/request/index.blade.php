@extends('spark::layouts.app')


@section('content')
<div class="container spark-screen">
  <div class="row">
    <div class="col-md-4">
      @include('frontend.request.services')
    </div>
    <div class="col-md-8">
      <div class="panel panel-default ">
        <div class="panel-heading">
          <h5>Request a service</h5>
        </div>
        <div class="panel-body">
          <div id="page">
          <script>
          var loadscripts = function(src, fallback){
               var s,r,t;
                r = false;
                s = document.createElement('script');
                s.type = 'text/javascript';
                s.src = src;
                s.onload = s.onreadystatechange = function() {
                //console.log( this.readyState ); //uncomment this line to see which ready states are called.
                if ( !r && (!this.readyState || this.readyState == 'complete') )
                {
                  r = true;
                  fallback();
                }
                };
                t = document.getElementsByTagName('script')[0];
                t.parentNode.insertBefore(s, t);
          
          }
          loadscripts('/js/jquery.inputmask.bundle.min.js', function(){
              $('input').inputmask();
              console.log('loading phone.js');
          });
          console.log();
          
          </script>
          <?php 
          $fields = [

                [
                    'name' => 'name',
                    'label' => 'Full name (middle inital is optional)',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'full_name'
                ],
                [
                    'name' => 'home_phone',
                    'label' => 'Home Phone Number',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'home_phone',
                    'data-inputmask' => "'alias': 'phone'"
                ],
                [
                    'name' => 'work_phone',
                    'label' => 'Work Phone Number',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'work_phone',
                    'data-inputmask' => "'alias': 'phone'"
                ],
                [
                    'name' => 'ssn',
                    'label' => 'Social Security Number',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'ssn',
                    'data-inputmask' => "'mask': '999-99-9999'"
                ],
                [
                    'name' => 'drivers_license_number',
                    'label' => 'Drivers License Number',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'drivers_license_number',
                ],
                [
                    'name' => 'other_names',
                    'label' => 'Other Names (aliases, seperated by commas or spaces)',
                    'type' => 'textarea',
                    'value' => '',
                    'id' => 'other_names'
                ],
                [
                    'name' => 'date_of_birth',
                    'label' => 'Date of Birth (MM/DD/YYYY)',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'drivers_license_number',
                    'data-inputmask' => "'alias': 'mm/dd/yyyy'"
                ],
                [
                    'name' => 'date_of_birth',
                    'label' => 'Date of Birth (MM/DD/YYYY)',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'date_of_birth',
                    'data-inputmask' => "'alias': 'mm/dd/yyyy'"
                ],
                [
                    'name' => 'married_since',
                    'label' => 'Married Since (MM/DD/YYYY) (optional)',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'married_since',
                    'data-inputmask' => "'alias': 'mm/dd/yyyy'"
                ],
                [
                    'name' => 'previous_landlord_name',
                    'label' => 'Previous Landlord Name',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'previous_landlord_name'
                ],
                [
                    'name' => 'previous_landlord_phone',
                    'label' => 'Previous Landlord Phone Number',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'previous_landlord_phone',
                    'data-inputmask' => "'alias': 'phone'"
                ],
                [
                    'name' => 'start_date',
                    'label' => 'Tenant Start Date',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'start_date',
                    'data-inputmask' => "'alias': 'mm/dd/yyyy'"
                ],
                [
                    'name' => 'address',
                    'label' => 'Current Address (just the street and house number)',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'address'
                ],
                [
                    'name' => 'zip',
                    'label' => 'Previous Zip (we use this to find the city and state)',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'previous_zip',
                    'data-inputmask' => "'mask': '[0-9]{5}(-[0-9]{4})'"
                ],
                [
                    'name' => 'tenant_type',
                    'label' => 'Previous Landlord Phone Number',
                    'type' => 'text',
                    'value' => '',
                    'id' => 'previous_landlord_phone'
                ],
                [
                    'name' => 'additional_names_on_lease',
                    'label' => 'Additional Names On Lease',
                    'type' => 'textarea',
                    'value' => '',
                    'id' => 'additional_names_on_lease'
                ],
            ];?>
            @foreach($fields as $field)
            <?php extract($field);
?>
            <div class="input-field">
                @if($type != 'textarea')
                <input type="{{$type}}" id="{{$id}}" value="{{$value}}" name="{{$name}}" @if(!empty($field['data-inputmask'])) data-inputmask="{{ $field['data-inputmask'] }}"@endif>
                @else 
                <{{$type}} id="{{$id}}" value="{{$value}}" name="{{$name}}" class="materialize-textarea" ></{{$type}}>
                @endif
                {!! !empty($label)?'<label for="'.$id.'">'. $label. '</label>':'' !!}
            </div>
            @endforeach
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection