<div class="input-field">
  <input type="text" id="full_name" value="" name="name">
  <label for="full_name">Full name (middle inital is optional)</label>
</div>
<div class="input-field">
  <input type="text" id="home_phone" value="" name="home_phone" data-inputmask="'alias': 'phone'">
  <label for="home_phone">Home Phone Number</label>
</div>
<div class="input-field">
  <input type="text" id="work_phone" value="" name="work_phone" data-inputmask="'alias': 'phone'">
  <label for="work_phone">Work Phone Number</label>
</div>
<div class="input-field">
  <input type="text" id="ssn" value="" name="ssn" data-inputmask="'mask': '999-99-9999'">
  <label for="ssn">Social Security Number</label>
</div>
<div class="input-field">
  <input type="text" id="drivers_license_number" value="" name="drivers_license_number">
  <label for="drivers_license_number">Drivers License Number</label>
</div>
<div class="input-field">
  <textarea id="other_names" value="" name="other_names" class="materialize-textarea"></textarea>
  <label for="other_names">Other Names (aliases, seperated by commas or spaces)</label>
</div>
<div class="input-field">
  <input type="text" id="drivers_license_number" value="" name="date_of_birth" data-inputmask="'alias': 'mm/dd/yyyy'">
  <label for="drivers_license_number">Date of Birth (MM/DD/YYYY)</label>
</div>
<div class="input-field">
  <input type="text" id="date_of_birth" value="" name="date_of_birth" data-inputmask="'alias': 'mm/dd/yyyy'">
  <label for="date_of_birth">Date of Birth (MM/DD/YYYY)</label>
</div>
<div class="input-field">
  <input type="text" id="married_since" value="" name="married_since" data-inputmask="'alias': 'mm/dd/yyyy'">
  <label for="married_since">Married Since (MM/DD/YYYY) (optional)</label>
</div>
<div class="input-field">
  <input type="text" id="previous_landlord_name" value="" name="previous_landlord_name">
  <label for="previous_landlord_name" class="">Previous Landlord Name</label>
</div>
<div class="input-field">
  <input type="text" id="previous_landlord_phone" value="" name="previous_landlord_phone" data-inputmask="'alias': 'phone'">
  <label for="previous_landlord_phone">Previous Landlord Phone Number</label>
</div>
<div class="input-field">
  <input type="text" id="start_date" value="" name="start_date" data-inputmask="'alias': 'mm/dd/yyyy'">
  <label for="start_date">Tenant Start Date</label>
</div>
<div class="input-field">
  <input type="text" id="address" value="" name="address">
  <label for="address">Current Address (just the street and house number)</label>
</div>
<div class="input-field">
  <input type="text" id="previous_zip" value="" name="zip" data-inputmask="'mask': '[0-9]{5}(-[0-9]{4})'">
  <label for="previous_zip">Previous Zip (we use this to find the city and state)</label>
</div>
<div class="input-field">
  <input type="text" id="previous_landlord_phone" value="" name="tenant_type">
  <label for="previous_landlord_phone">Previous Landlord Phone Number</label>
</div>
<div class="input-field">
  <textarea id="additional_names_on_lease" value="" name="additional_names_on_lease" class="materialize-textarea"></textarea>
  <label for="additional_names_on_lease">Additional Names On Lease</label>
</div>