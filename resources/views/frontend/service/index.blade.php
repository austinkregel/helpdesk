@extends('spark::layouts.app')


@section('content')
<div class="container spark-screen">
  <div class="row">
    <div class="col-md-4">
      @include('frontend.request.services')
    </div>
    <div class="col-md-8">
      <div class="panel panel-default ">
        <div class="panel-heading">
          <h5>Requesting a {{ $service_name }}</h5>
        </div>
        <div class="panel-body">
          <div id="page">
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection