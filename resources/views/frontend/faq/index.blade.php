<!-- Customers Heading -->
<div class="row">
    <div class="col-md-10 col-md-offset-1 splash-row-heading center-align">
        Our customers F.A.Q.
    </div>
</div>
<div class="row splash-features-icon-row">
    <div class="col-md-10 col-md-offset-1 center-align">
        <ul class="collapsible popout" data-collapsible="accordion">
        <?php $faq = App\Models\FAQ::all();?>
        @foreach($faq as $f)
        <li>
          <div class="collapsible-header">{{ $f->question }}</div>
          <div class="collapsible-body"><p>{{ $f->answer }}</p></div>
        </li>

        @endforeach
      </ul>
    </div>
</div>