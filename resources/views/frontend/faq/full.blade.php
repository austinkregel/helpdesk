@extends('spark::layouts.app')
<!-- Main Content -->
@section('content')
<style>
.col-md-10.col-md-offset-1.splash-row-heading.center-align{
    font-weight:700;
    font-size:1.4rem;
}
</style>
<div id="spark-terms-screen" class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div>
				<div class="panel-body">
	                @include('frontend.faq.index')

				</div>
			</div>
		</div>
	</div>
</div>
@endsection