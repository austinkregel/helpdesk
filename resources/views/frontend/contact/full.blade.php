@extends('spark::layouts.app')
<!-- Main Content -->
@section('content')
<style>
.col-md-10.col-md-offset-1.splash-row-heading.center-align{
    font-weight:700;
    font-size:1.4rem;
}
</style>
<div id="spark-terms-screen" class="container-fluid spark-screen">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
			    <div class="panel-heading">
			        If you have questions please ask!
			    </div>
				<div class="panel-body">
	                @include('frontend.contact.index')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection