 <!-- Customers Heading -->

<div class="row splash-features-icon-row">
    <div class="col-md-10 col-md-offset-1 center-align">
        <form class="col-md-12">
            <div class="row">
                <div class="input-field col-md-6 col-sm-12">
                    <input id="name" type="text" class="validate" value="{{ Auth::user()->name }}">
                    <label for="name">Name</label>
                </div>
                <div class="input-field col-md-6 col-sm-12">
                    <input id="email" type="email" class="validate" value="{{ Auth::user()->email }}">
                    <label for="email">Email</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col-sm-12">
                    <input id="company" type="text" class="validate" value="{{ Auth::user()->phone_number }}">
                    <label for="company">Phone Number</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col-sm-12">
                  <textarea name="message" id="message" class="materialize-textarea"></textarea>
                  <label for="message">Questions, Comments, Or concerns.</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                 <p class="right-align"><button class="btn btn-large waves-effect waves-light" type="button" name="action">Send Message</button></p>
                </div>
            </div>
        </form>
    </div>
</div>