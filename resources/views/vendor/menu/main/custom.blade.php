<?php 
$menu = new Kregel\Menu\Menu;
$menu = $menu->using('materializepjax');
        $menus =  $menu->config();?>
<div class="navbar-fixed" >
    <nav @if(config('app.debug')) class="themer--primary" @endif>
        {!! $menus->dropdowns !!}
        <div class="nav-wrapper">
            <?php $name = config('kregel.menu.brand.name');?>
                {{-- Here we must make sure that we are checking to see if the name is that of a closure
                     If it is, we need to call the closure. Otherwise we need to just spit out the data. --}}
                @if($name instanceof Closure)

                    <a class="brand-logo p-link" href="{{url(config('kregel.menu.brand.link'))}}"
                       style="padding-left: 10px;">{!! $name() !!}</a>
                @else
                    <a class="brand-logo p-link" href="{{url(config('kregel.menu.brand.link'))}}"
                       style="padding-left: 10px;">{!! $name !!}</a>
                @endif
				<ul class="right hide-on-med-and-down">
                {!!$menus->devour() !!}
            </ul>
            <ul id="slide-out" class="side-nav">
                {!! $menu->sideMenu() !!}
            </ul>
        </div>
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
    </nav>
</div>