function request(url, data, success, nochange, fail) {
    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
    xmlhttp.open("{{$type}}", url);

    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.setRequestHeader("X-Requested-With", 'XMLHttpRequest');
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4) {
            var response = JSON.parse(xmlhttp.responseText);
            switch (response.code) {
                case 200:
                case 202:
                    success(response);
                    break;
                case 205:
                    nochange(response);
                    break;
                default:
                    fail(response);
            }
        }
    };
    {{--xmlhttp.upload.onprogress = function(e) {--}}
        {{--if (e.lengthComputable) {--}}
            {{--var percentComplete = (e.loaded / e.total) * 100;--}}
            {{--console.log(percentComplete + '% uploaded');--}}
        {{--}--}}
    {{--};--}}
    xmlhttp.send(JSON.stringify(data));
}