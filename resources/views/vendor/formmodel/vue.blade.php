<script>
    var data = {
        response: '',
        debug:'',
        data: {
            @if(config('kregel.formmodel.using.csrf'))
            _token:"{{csrf_token()}}",
            @endif
            <?php
                $count = count($components);
                $i =0;
            foreach($components as $c){
                echo "\t".$c.': \'\''.((($count - 1) == $i)?'':','). "\n";
                $i++;
            }?>
        }
    };
    var vm;
    vm = new Vue({
        el: "#vue-form-wrapper",
        data: data,
        methods: {
            makeRequest: function (e) {
                e.preventDefault();
                var responseArea = document.getElementById('response');
                request(e.target.action,
                        this.$data.data
                , function(response){
                    responseArea.classList = 'alert alert-success';
                    @if($type === 'delete') $(form).parent().parent().parent().remove();@endif
                    Materialize.toast(response.message, 4000, 'green')
                }, function(response){
                    responseArea.classList = 'alert alert-warning';
                    Materialize.toast(response.message, 4000, 'amber grey-text text-darken-4')
                }, function(response) {
                    responseArea.classList = 'alert alert-danger';
                    Materialize.toast(response.message, 4000, 'red')
                })
            },
            close: function (e) {
                this.response = '';
            },
            updateSelect: function(){
                
            }
        },
        ready: function(){

        }
    });
    @include('formmodel::request', ['type' => $type])
</script>
