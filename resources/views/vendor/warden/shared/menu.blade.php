<div class="panel panel-default panel-flush">
    <div class="panel-heading @if(config('app.debug')) themer--secondary @endif">
        Edit some things in the DB
    </div>
    <div class="spark-panel-body panel-body">
        <div class="spark-settings-tabs">
            <ul class="nav-wrapper nav spark-settings-tabs-stacked" role="tablist">
                <?php
                $i = 2000;
                $dropdown = '';
                ?>
                @foreach(config('kregel.warden.models') as $menuitem => $classname)
                        <!-- Settings Dropdown -->
                <!-- Authenticated Right Dropdown -->
                @if(Auth::user()->can('edit-'.$menuitem))
                    <li class="dropdown">
                        <a href="#" class="dropdown-button @if(config('app.debug')) themer--secondary @endif"
                           data-activates="side-bar-menu-{{ $i }}">
                            {{ ucwords($menuitem) }} <i class="material-icons right">arrow_drop_down</i>
                        </a>
                        <?php
                        $dropdown .= '
                    <ul class="dropdown-content" id="side-bar-menu-' . $i . '">
                        <!-- Settings -->
                        <li>
                            <a href="' . route('warden::new-model', $menuitem) . '" class="p-link">
                                <i class="fa fa-btn fa-fw fa-cog"></i>New ' . ucwords($menuitem) . '
                            </a>
                        </li>
                        <li>
                            <a href="' . route('warden::models', $menuitem) . '" class="p-link">
                                <i class="fa fa-btn fa-fw fa-cog"></i>List all ' . ucwords($menuitem) . 's
                            </a>
                        </li>
                    </ul>';$i++;
                        ?>
                    </li>
                    @endcan
                    @endforeach
            </ul>
            {!! $dropdown !!}
        </div>
    </div>
</div>