@extends(config('kregel.warden.views.base-layout'))

@section('errors')
    @include('warden::shared.errors')
@stop

@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-4">
            @include('warden::shared.menu')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default ">
                <div class="panel-heading">
                    <h5>Current stats about your site:</h5>
                </div>
                <div class="panel-body">
                    @foreach(config('kregel.warden.models') as $menuitem => $classname)
                        <?php
                        $model = $classname['model'];
                        $count = $model::all()->count(); 
                        ?>
                        <div class="col-md-12">There {{ ( $count == 1?'is':'are')  }} {{ $count }} {{$menuitem . ( $count == 1?'':'s') }}</div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection