@extends(config('kregel.warden.views.base-layout'))

@section('errors')
    @include('warden::shared.errors')
@stop

@section('content')
    <div class="container spark-screen">
        <div class="row">
            <div class="col-md-4">
                @include('warden::shared.menu')
            </div>
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading @if(config('app.debug')) themer--secondary @endif">
                        <h5>You're upserting a {{ ucwords($model_name) }}</h5>
                    </div>
                    <div class="panel-body @if(config('app.debug')) themer--secondary @endif">
                        <div id="page">
                            {!! $form !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('body_script')
    @include('formmodel::vue', ['components' => $vue_components, 'type' =>$method])
@endsection