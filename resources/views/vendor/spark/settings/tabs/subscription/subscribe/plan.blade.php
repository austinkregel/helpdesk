<!-- Settings Subscription Plan Selector -> Single Plan Block -->
<label>
    <!-- Radio Button / Plan Name -->
    <input type="radio" name="plan" value="@{{ plan.id }}" v-model="forms.subscribe.plan">

    &nbsp;

    @{{ plan.name }}

            <!-- Plan Price / Interval -->
    (@{{ plan.currencySymbol }}@{{ plan.price }} / @{{ plan.interval | capitalize }})

    &nbsp;

    <!-- Plan Features Tooltip -->
    <i style="font-size: 16px;"
       class="fa fa-info-circle tooltipped"
       data-position="right"
       data-delay="50"
       data-tooltip="@{{ getPlanFeaturesForTooltip(plan) }}">
    </i>
</label>
