<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{ url('css/app.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>

    </style>
</head>
<body>
    <!-- Main Content -->
    <div class="container-fluid spark-splash-screen">
        <!-- Branding / Navigation -->
        <div class="row splash-nav navbar-fixed">
            <div class="col-md-10 col-md-offset-1">
                <div class="pull-left splash-brand " style="margin-top:5px;">
                    <?php $name = config('kregel.menu.brand.name');?>

                    @if($name instanceof Closure)
                        <a class="brand-logo p-link" href="{{url(config('kregel.menu.brand.link'))}}"
                           style="padding-left: 10px;">{!! $name() !!}</a>
                    @else
                        <a class="brand-logo p-link" href="{{url(config('kregel.menu.brand.link'))}}"
                           style="padding-left: 10px;">{!! $name !!}</a>
                    @endif
                </div>
                <div id="primary-nav" class="navbar-collapse collapse splash-nav-list">
                    <ul class="nav navbar-nav navbar-right inline-list">
        
                        <li data-scroll class="splash-nav-link active"><a href="#pricing">Pricing</a></li>
                        <li data-scroll class="splash-nav-link"><a href="#faq">F.A.Q.</a></li>
        
                        @if(Auth::check())
                        <li class="splash-nav-link splash-nav-link-highlight-border"><a href="/home">Home</a></li>
                        @else
                        <li class="splash-nav-link splash-nav-link-highlight"><a href="/login">Login</a></li>
                        <li class="splash-nav-link splash-nav-link-highlight-border"><a href="/register">Register</a></li>
                        @endif                    </ul>
                </div>
        
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="container">
            <!-- Inspiration -->
            <div class="row splash-inspiration-row">
                <div class="col-md-4 col-md-offset-1">
                    <div id="splash-inspiration-heading">
                        Lower your stress!
                    </div>
    
                    <div id="splash-inspiration-text">
                        {{-- We will make sure you can keep more of your rental housing business income by offering
                        you a tenant management system that will be able to fit the needs of your business.--}}
                        
                        Lower your risk and your stress by having us check out your prospective renters! We can 
                        do a full suite of checking and investigation on the person about to rent from you. 
                    </div>
                </div>
    
                <!-- Browser Window -->
                <div class="col-md-6" class="splash-browser-window-container">
                    <div class="splash-browser-window">
                        <div class="splash-browser-dots-container">
                            <ul class="list-inline splash-browser-dots">
                                <li><i class="fa fa-circle red"></i></li>
                                <li><i class="fa fa-circle yellow"></i></li>
                                <li><i class="fa fa-circle green"></i></li>
                            </ul>
                        </div>
                        <div>
                            <img src="https://placehold.co/550x400" style="width: 100%;">
                        </div>
                    </div>
                </div>
            </div>
    
            <!-- Features Heading -->
            <div class="row">
                <div class="col-md-10 col-md-offset-1 splash-row-heading center-align">
                    We offer it all!
                </div>
            </div>
    
            <!-- Feature Icons -->
            <div class="row splash-features-icon-row">
                <div class="col-md-10 col-md-offset-1 center-align">
                    <div class="col-md-4 splash-features-feature">
                        <div class="splash-feature-icon">
                            <i class="fa fa-lock"></i>
                        </div>
    
                        <div class="splash-feature-heading">
                            Credit reports
                        </div>
    
                        <div class="splash-feature-text">
                            Need to know if they are finanically stable? No problem! 
                        </div>
                    </div>
    
                    <div class="col-md-4 splash-features-feature">
                        <div class="splash-feature-icon">
                            <i class="fa fa-money"></i>
                        </div>
    
                        <div class="splash-feature-heading">
                            Subscriptions
                        </div>
    
                        <div class="splash-feature-text">
                            Pay us once a month, and you will have access to us 24 / 7
                        </div>
                    </div>
    
                    <div class="col-md-4 splash-features-feature">
                        <div class="splash-feature-icon">
                            <i class="fa fa-phone"></i>
                        </div>
    
                        <div class="splash-feature-heading">
                            Two-Factor Security
                        </div>
    
                        <div class="splash-feature-text">
                            Enable two-factor authentication and your account will never be more secure!
                        </div>
                    </div>
                </div>
            </div>
    
            <!-- Feature Icons -->
            <div class="row splash-features-icon-row">
                <div class="col-md-10 col-md-offset-1 center-align">
                    <div class="col-md-4 splash-features-feature">
                        <div class="splash-feature-icon">
                            <i class="fa fa-users"></i>
                        </div>
    
                        <div class="splash-feature-heading">
                            Join Your Team
                        </div>
    
                        <div class="splash-feature-text">
                            Spark provides simple, built-in support for creating and managing teams and invitations.
                        </div>
                    </div>
    
                    <div class="col-md-4 splash-features-feature">
                        <div class="splash-feature-icon">
                            <i class="fa fa-cubes"></i>
                        </div>
    
                        <div class="splash-feature-heading">
                            Make It Your Own
                        </div>
    
                        <div class="splash-feature-text">
                            Customize or replace any of the views provided with Spark and make the design your own.
                        </div>
                    </div>
    
                    <div class="col-md-4 splash-features-feature">
                        <div class="splash-feature-icon">
                            <i class="fa fa-clock-o"></i>
                        </div>
    
                        <div class="splash-feature-heading">
                            Save Weeks Of Work
                        </div>
    
                        <div class="splash-feature-text">
                            Get started writing what's unique: your application. Don't worry about subscription and authentication logic.
                        </div>
                    </div>
                </div>
            </div>
    
            <!-- Pricing Variables -->
            <?php $plans = Spark::plans()->monthly()->active(); ?>
    
            <?php
                switch (count($plans)) {
                    case 0:
                    case 1:
                        $columns = 'col-md-6 col-md-offset-3';
                        break;
                    case 2:
                        $columns = 'col-md-6';
                        break;
                    case 3:
                        $columns = 'col-md-4';
                        break;
                    case 4:
                        $columns = 'col-md-3';
                        break;
                    default:
                        throw new Exception("Unsupported number of plans. Please customize view.");
                }
            ?>
    
            <!-- Pricing Heading -->
            @if (count($plans) > 0)
                <div class="row" id="pricing">
                    <div class="col-md-10 col-md-offset-1 splash-row-heading center-align">
                        Simple Pricing
                    </div>
                </div>
    
                <!-- Pricing Table -->
                <div class="row splash-pricing-table-row center-align">
                    <div class="col-md-10 col-md-offset-1">
                        @foreach ($plans as $plan)
                            @if ($plan->isActive())
                                <div class="{{ $columns }}">
                                    <div class="panel panel-default spark-plan-{{ $plan->id }}">
                                        <div class="panel-heading splash-plan-heading">
                                            {{ $plan->name }}
                                        </div>
    
                                        <div class="panel-body">
                                            <ul class="splash-plan-feature-list">
                                                @foreach ($plan->features as $feature)
                                                    <li>{{ $feature }}</li>
                                                @endforeach
                                            </ul>
    
                                            <hr>
    
                                            <div class="splash-plan-price">
                                                {{ $plan->currencySymbol }}{{ $plan->price }}
                                            </div>
    
                                            <div class="splash-plan-interval">
                                                {{ $plan->interval }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
    
                <!-- Call To Action Button -->
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 center-align">
                        <a href="/register">
                            <button class="btn btn-primary splash-get-started-btn">
                                Get Started!
                            </button>
                        </a>
                    </div>
                </div>
            @endif
            
            <!-- Customers Heading -->
            <div class="row">
                <div class="col-md-10 col-md-offset-1 splash-row-heading center-align">
                    What Our Customers Say
                </div>
            </div>
    
            <!-- Customer Testimonials -->
            <div class="row splash-customer-row">
                <div class="col-md-10 col-md-offset-1 center-align">
                    <div class="col-md-4 splash-customer">
                        <div class="splash-customer-avatar">
                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/msurguy/128.jpg">
                        </div>
    
                        <div class="splash-customer-quote">
                            This is an inspiring testimonial about your application.
                        </div>
    
                        <div class="splash-customer-identity">
                            <div class="splash-customer-name">Maksim Surguy</div>
                            <div class="splash-customer-title">CEO, Company</div>
                        </div>
                    </div>
    
                    <div class="col-md-4 splash-customer">
                        <div class="splash-customer-avatar">
                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/allisongrayce/128.jpg">
                        </div>
    
                        <div class="splash-customer-quote">
                            This is an inspiring testimonial about your application.
                        </div>
    
                        <div class="splash-customer-identity">
                            <div class="splash-customer-name">Allison Grayce</div>
                            <div class="splash-customer-title">CEO, Company</div>
                        </div>
                    </div>
    
                    <div class="col-md-4 splash-customer">
                        <div class="splash-customer-avatar">
                            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/richcsmith/128.jpg">
                        </div>
    
                        <div class="splash-customer-quote">
                            This is an inspiring testimonial about your application.
                        </div>
    
                        <div class="splash-customer-identity">
                            <div class="splash-customer-name">Rich Smith</div>
                            <div class="splash-customer-title">CEO, Company</div>
                        </div>
                    </div>
                </div>
            </div>
            @if(\Auth::check())
            <div class="row">
                <div class="col-md-10 col-md-offset-1 splash-row-heading center-align">
                    If you have questions please ask!
                </div>
            </div>
            @include('frontend.contact.index')
            @endif
            <div id="faq">
            @include('frontend.faq.index')
            </div>
            <!-- Footer -->
            <div class="row">
                <!-- Company Information -->
                <div class="col-md-10 col-md-offset-1 splash-footer">
                    <div class="pull-left splash-footer-company">
                        Copyright © {{ Spark::company() }} - <a href="/terms">Terms Of Service</a>
                    </div>
    
                    <!-- Social Icons -->
                    <div class="pull-right splash-footer-social-icons">
                        <a href="http://facebook.com">
                            <i class="fa fa-btn fa-facebook-square"></i>
                        </a>
                        <a href="http://twitter.com">
                            <i class="fa fa-btn fa-twitter-square"></i>
                        </a>
                        <a href="http://github.com">
                            <i class="fa fa-github-square"></i>
                        </a>
                    </div>
    
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="{{ url('js/m.js') }}"></script>
    <script src="{{ url('js/smooth.js') }}"></script>
    <script>
        var header = document.querySelector(".navbar-fixed");
    window.addEventListener('scroll', function (e) {
        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
            shrinkOn = 10;
        if (distanceY > shrinkOn) {
            if (!header.classList.contains('smaller')) header.className = 'smaller ' + header.className;
        } else {
            if (header.classList.contains("smaller")) {
                header.className = header.className.replace(/\bsmaller\b\s/g, '');
            }
        }
    });

    </script>
</body>
</html>
