<div class="panel panel-default panel-flush">
                <div class="panel-heading">
                    Edit the permissions for users and roles
                </div>
                <div class="spark-panel-body panel-body">
                    <div class="spark-settings-tabs">
                        <ul class="nav-wrapper nav spark-settings-tabs-stacked" role="tablist">
                           <?php $i =0; $dropdown = '';?>
                           @foreach(['user','role', 'permission'] as $menuitem)
                                    <!-- Settings Dropdown -->
                                    
                            <!-- Authenticated Right Dropdown -->
                            @if(Auth::user()->can('edit-'.$menuitem))
                            <li class="dropdown">
                                <a href="#" class="dropdown-button" data-activates="side-bar-menu-{{ $i }}" >
                                    {{ ucwords($menuitem) }} <i class="material-icons right">arrow_drop_down</i>
                                </a>
                                <?php
                                $dropdown .= '<ul class="dropdown-content" id="side-bar-menu-'. $i .'">
                                    <!-- Settings -->
                                    <li>
                                        <a href="' . route('entrust-gui::' . $menuitem . 's.create') .'" class="p-link">
                                            <i class="fa fa-btn fa-fw fa-cog"></i>New ' .ucwords($menuitem) .'
                                        </a>
                                    </li>
                                    <li>
                                        <a href="' . route('entrust-gui::' . $menuitem . 's.index') .'" class="p-link">
                                            <i class="fa fa-btn fa-fw fa-cog"></i>View all '.ucwords($menuitem).'s
                                        </a>
                                    </li>
                                </ul>';
                                $i++;
                                ?>
                            </li>
                            @endcan
                            @endforeach
                        </ul>
                        {!! !empty($dropdown)?$dropdown:'' !!}
                    </div>
                </div>
            </div>