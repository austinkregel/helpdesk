@extends(Config::get('entrust-gui.layout'))

@section('heading', 'Edit User')

@section('content')
    <div class="container spark-screen">
        <div class="col-md-4">
            @include('entrust-gui::partials.navigation')
        </div>
            <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Edit User</h5>
                </div>
                <div class="panel-body">
                   <div class="col-md-10 col-md-offset-1 col-sm-offset-0">
                    <form action="{{ route('entrust-gui::users.update', $user->id) }}" method="post">
                        <input type="hidden" name="_method" value="put">
                        @include('entrust-gui::users.partials.form')
                        <button type="submit" id="save" class="btn btn-labeled btn-primary"><span class="btn-label"><i
                                        class="fa fa-check"></i></span>{{ trans('entrust-gui::button.save') }}</button>
                        <a class="btn btn-labeled btn-default" href="{{ route('entrust-gui::users.index') }}"><span
                                    class="btn-label"><i
                                        class="fa fa-chevron-left"></i></span>{{ trans('entrust-gui::button.cancel') }}</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

