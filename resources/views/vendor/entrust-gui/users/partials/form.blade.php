<div class="row">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="input-field">
        <input type="text" class="validate" id="name" name="name" value="{{ (Session::has('errors')) ? old('name', '') : $user->name }}">
        <label for="name">Name</label>
    </div>
    <div class="input-field">
        <input type="email" class="validate" id="email" name="email" value="{{ (Session::has('errors')) ? old('email', '') : $user->email }}">
        <label for="email">Email address</label>
    </div>
    <div class="input-field">
        
        <input type="password" class="validate" id="password" name="password">
        <label for="password">Password</label>
        @if(Route::currentRouteName() == 'entrust-gui::users.edit')
            <div class="alert alert-info">
              <span class="fa fa-info-circle"></span> Leave the password field blank if you wish to keep it the same.
            </div>
        @endif
    </div>
    @if(Config::get('entrust-gui.confirmable') === true)
    <div class="input-field">
        <label for="password">Confirm Password</label>
        <input type="password" class="validate" id="password_confirmation" name="password_confirmation">
    </div>
    @endif
    <div class="input-field">
        <select name="roles[]" id="roles" multiple>
          <option value="" disabled selected>Please select 1 or more roles.</option>
            @foreach($roles as $index => $role)
                <option value="{{ $index }}" >{{ $role }}</option>
            @endforeach
        </select>
        <label>Role Selection</label>
    </div>
</div>