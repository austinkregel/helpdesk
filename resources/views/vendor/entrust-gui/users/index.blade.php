@extends(Config::get('entrust-gui.layout'))

@section('heading', 'Users')

@section('content')
    <div class="container spark-screen">
        <div class="col-md-4">
            @include('entrust-gui::partials.navigation')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Create User</h5>
                </div>
                <div class="panel-body">
                    <div class="models--actions">
                        <a class="btn btn-labeled btn-primary" href="{{ route('entrust-gui::users.create') }}"><span
                                    class="btn-label"><i
                                        class="fa fa-plus"></i></span>{{ trans('entrust-gui::button.create-user') }}
                        </a>
                    </div>
                    <table class="striped">
                        <tr>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                        @foreach($users as $user)
                            <tr>
                                <td>
                                {{ $user->email }}
                                </td>
                                <td style="width:250px;">
                                    <form action="{{ route('entrust-gui::users.destroy', $user->id) }}" method="post" style="display:inline">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <a class="btn btn-labeled btn-default waves-effect waves-light"
                                           href="{{ route('entrust-gui::users.edit', $user->id) }}"><span
                                                    class="left"><i
                                                        class="fa fa-pencil"></i></span>{{ trans('entrust-gui::button.edit') }}
                                        </a>
                                        <button type="submit" class="waves-effect waves-light btn red darken-1 black-text"><span class="left"><i
                                                        class="fa fa-trash"></i></span>{{ trans('entrust-gui::button.delete') }}
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="text-center">
                        @include('frontend.material.pagination', ['paginator' => $users])
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
