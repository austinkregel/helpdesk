@extends(Config::get('entrust-gui.layout'))

@section('heading', 'Permissions')

@section('content')
<div class="container spark-screen">
        <div class="col-md-4">
            @include('entrust-gui::partials.navigation')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Create User</h5>
                </div>
                <div class="panel-body">
                    <div class="models--actions">
                        <a class="btn btn-labeled btn-primary" href="{{ route('entrust-gui::permissions.create') }}"><span class="btn-label"><i class="fa fa-plus"></i></span>{{ trans('entrust-gui::button.create-permission') }}</a>
                      </div>
                    <table class="table table-striped">
                        <tr>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        @foreach($models as $model)
                            <tr>
                                <td>{{ $model->name }}</th>
                                <td style="width:250px;">
                                    <form action="{{ route('entrust-gui::permissions.destroy', $model->id) }}" method="post">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <a class="btn waves-effect waves-light " href="{{ route('entrust-gui::permissions.edit', $model->id) }}"><span class="btn-label"><i class="fa fa-pencil"></i></span>{{ trans('entrust-gui::button.edit') }}</a>
                                        <button type="submit" class="btn waves-effect waves-light red darken-1 black-text"><span class="btn-label"><i class="fa fa-trash"></i></span>{{ trans('entrust-gui::button.delete') }}</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="text-center">
                        @include('frontend.material.pagination', ['paginator' => $models])
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
@endsection
