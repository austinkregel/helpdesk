<div class="row">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="input-field">
        <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ (Session::has('errors')) ? old('name', '') : $model->name }}">
        <label for="name">Name</label>
    </div>
    <div class="input-field">
        <input type="text" class="form-control" id="display_name" placeholder="Display Name" name="display_name" value="{{ (Session::has('errors')) ? old('display_name', '') : $model->display_name }}">
        <label for="display_name">Display Name</label>
    </div>
    <div class="input-field">
        <input type="text" class="form-control" id="description" placeholder="Description" name="description" value="{{ (Session::has('errors')) ? old('description', '') : $model->description }}">
        <label for="description">Description</label>
    </div>
    <div class="input-field">
        <select name="roles[]" multiple >
            @foreach($relations as $index => $relation)
                <option value="{{ $index }}" {{ ((in_array($index, old('roles', []))) || ( ! Session::has('errors') && $model->roles->contains('id', $index))) ? 'selected="true"' : '' }}>{{ $relation }}</option>
            @endforeach
        </select>
        <label for="roles">Roles</label>
    </div>
</div>