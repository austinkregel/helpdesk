@extends(Config::get('entrust-gui.layout'))

@section('heading', 'Create Permission')

@section('content')

    <div class="container spark-screen">
        <div class="col-md-4">
            @include('entrust-gui::partials.navigation')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Create Permission</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-10 col-md-offset-1 col-sm-offset-0">
                                                        
                        <form action="{{ route('entrust-gui::permissions.store') }}" method="post" role="form">
                            @include('entrust-gui::permissions.partials.form')
                            <button type="submit" class="btn btn-labeled btn-primary"><span class="btn-label"><i class="fa fa-plus"></i></span>{{ trans('entrust-gui::button.create') }}</button>
                            <a class="btn " href="{{ route('entrust-gui::permissions.index') }}"><span class="btn-label"><i class="fa fa-chevron-left"></i></span>{{ trans('entrust-gui::button.cancel') }}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
