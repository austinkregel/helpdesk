<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="input-field">
    <input type="text" id="name" placeholder="Name" name="name" value="{{ (Session::has('errors')) ? old('name', '') : $model->name }}">
    <label for="name">Name</label>
</div>
<div class="input-field">
    <input type="text" id="display_name" placeholder="Display Name" name="display_name" value="{{ (Session::has('errors')) ? old('display_name', '') : $model->display_name }}">
    <label for="display_name">Display Name</label>
</div>
<div class="input-field">
    <input type="text" id="description" placeholder="Description" name="description" value="{{ (Session::has('errors')) ? old('description', '') : $model->description }}">
    <label for="description">Description</label>
</div>
<div class="input-field">
    <select name="permissions[]" multiple>
      @foreach($relations as $index => $relation)
        <option value="{{ $index }}" {{ ((in_array($index, old('permissions', []))) || ( ! Session::has('errors') && $model->perms->contains('id', $index))) ? 'selected' : '' }}>{{ $relation }}</option>
      @endforeach
    </select>
    <label for="permissions">Permissions</label>
</div>
