@extends(Config::get('entrust-gui.layout'))

@section('heading', 'Create Role')

@section('content')
    <div class="container spark-screen">
        <div class="col-md-4">
            @include('entrust-gui::partials.navigation')
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Create Role</h5>
                </div>
                <div class="panel-body">
                    <div class="col-md-10 col-md-offset-1 col-sm-offset-0">
                                
                        <form action="{{ route('entrust-gui::roles.store') }}" method="post" role="form">
                            @include('entrust-gui::roles.partials.form')
                            <button type="submit" class="btn btn-labeled btn-primary"><span class="btn-label"><i class="fa fa-plus"></i></span>{{ trans('entrust-gui::button.create') }}</button>
                            <a class="btn btn-labeled btn-default" href="{{ route('entrust-gui::roles.index') }}"><span class="btn-label"><i class="fa fa-chevron-left"></i></span>{{ trans('entrust-gui::button.cancel') }}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
