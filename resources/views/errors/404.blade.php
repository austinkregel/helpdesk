@extends('spark::layouts.app')
<!-- Main Content -->
@section('content')
<style>
  
html, body{height:100%; margin:0;padding:0}
 
.container-fluid> div{
  height:100%;
  display:table;
  width: 100%;
  text-align:center;
  padding: 0;
}
 
.valign-wrapper {height: 100%; display:table-cell; vertical-align: middle;}
 
.valign {
  float:none;
  margin:0 auto;
}
</style>
<div class="container-fluid spark-screen">
	<div style="position:absolute;top:0;left:0;right:0;bottom:0;">
    	<div class="valign-wrapper">
    		<h3 class="valign">
    		        OH NO!!! That's an error!
    		</h3>
    	    <h5 class="valign">
                <pre style="font-size:12px;text-align:left;">
                <code>
                    {{ $exception }}
                </code>
                </pre>
    		</h5>
    	</div>
    </div>
</div>
@endsection