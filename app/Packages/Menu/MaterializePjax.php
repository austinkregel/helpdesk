<?php

namespace App\Packages\Menu;

use Auth;
use Kregel\Menu\Interfaces\AbstractMenu;

class MaterializePjax extends AbstractMenu
{
    public $menu = '';
    public $dropdowns = '';
    private $menuCount = 0;

    public function config()
    {
        $this->menu = '';
        $this->menu .= $this->add(config('kregel.menu.items'));
        if (config('kregel.menu.login.enabled')) {
            if (Auth::check()) {
                $this->menu .= $this->add(config('kregel.menu.login.sign-out'));
            } else {
                $this->menu .= $this->add(config('kregel.menu.login.sign-in'));
            }
        }
        return $this;
    }

    public function add(Array $options)
    {
        $tmpmenu = '';
        // die(var_dump($options));
        foreach ($options as $inner_text => $linkIconArray) {
            $perm = (empty($inner_text) || strtolower($inner_text) === strtolower('logout'));
            $user_can = \Auth::check();
            if($user_can){
                $user = \Auth::user();
                if($user->can('view-'. $perm) || $user->hasRole('saycomputer-admin') || $user->hasRole('developer')){
                    $tmpmenu .= $this->buildThings($inner_text, $linkIconArray);
                }
            }else{
                $tmpmenu .= $this->buildThings($inner_text, $linkIconArray);
            }
        }
        return $tmpmenu;
    }

    private function buildThings($inner_text, $linkIconArray){
        if (is_array($linkIconArray) && !isset($linkIconArray['link']) && !isset($linkIconArray['icon'])) {
            return $this->adddropdown($inner_text, $linkIconArray, ['submenu']);
        } elseif (is_object($linkIconArray)) {
            return $this->adddropdown($inner_text, (array)$linkIconArray, ['submenu']);
        } else {
            return $this->build($inner_text, $linkIconArray);
        }
    }
    public function addDropdown($dropdown_name, $elements, $classes = [])
    {
        $this->menuCount++;
        $this->dropdowns .= '<ul class="dropdown-content" id="dropdown-' . $this->menuCount . '">
            ' . $this->add($elements) . '
        </ul>';
        return '<li class="' . implode(' ', $classes) . '">
            <a href="#" class="dropdown-button waves-effect waves-light" data-activates="dropdown-' . $this->menuCount . '" role="button" aria-haspopup="true" aria-expanded="false">' . $dropdown_name . ' <i class="material-icons right">arrow_drop_down</i></a>
        </li>';
    }

    public function build($item_name, $menu, $attributes = [])
    {
        // die(var_dump($menu));
        if (!empty($menu['icon'])) {
            if (is_array($menu['icon'])) {
                $icon = implode(' ', $menu['icon']);
            } else {
                $icon = $menu['icon'];
            }
        }
        if (!is_array($menu)) {
            return '<li class="divider"></li>';
        }

        return '<li>
                <a ' . $this->attributes([
            'href' => $this->linkBuilder($menu['link']),
            'class' => 'p-link waves-effect'
        ]) . '>
               ' . (!empty($icon) ? '<i ' . $this->attributes(['class' => $icon]) . '></i>
                &nbsp;' : '') . $item_name . '
              </a>
            </li>
        ';
    }

    public function sideMenu(){
        $side = new MaterializeSideNav();
        return $side->config()->devour();
    }

    /**
     * This returns the value of the menu.
     *
     * @return String
     */
    public function devour()
    {
        return $this->menu;
    }

    protected function buildMenu($menu)
    {
        $this->menu = $this->add($menu);
        return $this;
    }
}
