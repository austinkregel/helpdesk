<?php

namespace App\Packages\Menu;

use Auth;
use Kregel\Menu\Interfaces\AbstractMenu;

class MaterializeSideNav extends MaterializePjax
{
    public $menu = '';
    public $dropdowns = '';
    private $menuCount = 0;

    public function addDropdown($dropdown_name, $elements, $classes = [])
    {
        $this->menuCount++;

        return '<li class="no-padding">
      <ul class="collapsible collapsible-accordion">
        <li>
          <a class="collapsible-header ' . implode(' ', $classes) . '">
          ' . $dropdown_name . ' <i class="material-icons right">arrow_drop_down</i></a>
            <div class="collapsible-body">
                <ul>
                ' . $this->add($elements) . '
                </ul>
            </div>
        </li>
        ';
    }

    public function sideMenu()
    {
        $this->menuCount = 1000;
        $menu = $this->add(config('kregel.menu.items'));
        if (config('kregel.menu.login.enabled')) {
            if (Auth::check()) {
                $menu .= $this->add(config('kregel.menu.login.sign-out'));
            } else {
                $menu .= $this->add(config('kregel.menu.login.sign-in'));
            }
        }
        return $menu;
    }

    /**
     * This returns the value of the menu.
     *
     * @return String
     */
    public function devour()
    {
        return $this->menu;
    }


}
