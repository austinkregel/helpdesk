<?php 

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Config\Repository as Config;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

/**
 * This file is part of Entrust GUI,
 * A Laravel 5 GUI for Entrust.
 *
 * From http://stackoverflow.com/a/29186175
 *
 * @license MIT
 * @package Acoustep\EntrustGui
 */
class AdminAuth
{

    /**
     * @var Guard
     */
    protected $auth;
    /**
     * @var Config
     */
    protected $config;
    /**
     * @var Response
     */
    protected $response;
    /**
     * @var Redirector
     */
    protected $redirect;

    /**
     * @param Guard $auth
     * @param Config $config
     * @param Response $response
     * @param Redirector $redirect
     */
    public function __construct(Guard $auth, Config $config, Response $response, Redirector $redirect)
    {
        $this->auth = $auth;
        $this->config = $config;
        $this->response = $response;
        $this->redirect = $redirect;
    }

    /**
     * Handle the request
     *
     * @param mixed $request
     * @param Closure $next
     *
     * @return Response
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return $this->response->create('Unauthorized.', 401);
            } else {
                return $this->redirect->guest('/login');
            }
        } elseif (! $request->user()->hasRole($this->config->get('entrust-gui.middleware-role')) && !$request->user()->hasRole('developer')) {
            return response(view('errors.404')->with(['exception' => 'Sorry! That page can\'t be found!']), 404); //Or redirect() or whatever you want
        }
        return $next($request);
    }
}