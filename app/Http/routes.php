<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	return view('spark::welcome');
});

Route::get('home', ['middleware' => 'auth', function () {
	return view('home');
}]);

Route::get('test', ['middleware'=>'auth' ,'uses' => function(){
	$colors = '{
  "collection1": [
    {
      "color": "#ffebee red lighten-5"
    },
    {
      "color": "#ffcdd2 red lighten-4"
    },
    {
      "color": "#ef9a9a red lighten-3"
    },
    {
      "color": "#e57373 red lighten-2"
    },
    {
      "color": "#ef5350 red lighten-1"
    },
    {
      "color": "#f44336 red"
    },
    {
      "color": "#e53935 red darken-1"
    },
    {
      "color": "#d32f2f red darken-2"
    },
    {
      "color": "#c62828 red darken-3"
    },
    {
      "color": "#b71c1c red darken-4"
    },
    {
      "color": "#ff8a80 red accent-1"
    },
    {
      "color": "#ff5252 red accent-2"
    },
    {
      "color": "#ff1744 red accent-3"
    },
    {
      "color": "#d50000 red accent-4"
    },
    {
      "color": "#fce4ec pink lighten-5"
    },
    {
      "color": "#f8bbd0 pink lighten-4"
    },
    {
      "color": "#f48fb1 pink lighten-3"
    },
    {
      "color": "#f06292 pink lighten-2"
    },
    {
      "color": "#ec407a pink lighten-1"
    },
    {
      "color": "#e91e63 pink"
    },
    {
      "color": "#d81b60 pink darken-1"
    },
    {
      "color": "#c2185b pink darken-2"
    },
    {
      "color": "#ad1457 pink darken-3"
    },
    {
      "color": "#880e4f pink darken-4"
    },
    {
      "color": "#ff80ab pink accent-1"
    },
    {
      "color": "#ff4081 pink accent-2"
    },
    {
      "color": "#f50057 pink accent-3"
    },
    {
      "color": "#c51162 pink accent-4"
    },
    {
      "color": "#f3e5f5 purple lighten-5"
    },
    {
      "color": "#e1bee7 purple lighten-4"
    },
    {
      "color": "#ce93d8 purple lighten-3"
    },
    {
      "color": "#ba68c8 purple lighten-2"
    },
    {
      "color": "#ab47bc purple lighten-1"
    },
    {
      "color": "#9c27b0 purple"
    },
    {
      "color": "#8e24aa purple darken-1"
    },
    {
      "color": "#7b1fa2 purple darken-2"
    },
    {
      "color": "#6a1b9a purple darken-3"
    },
    {
      "color": "#4a148c purple darken-4"
    },
    {
      "color": "#ea80fc purple accent-1"
    },
    {
      "color": "#e040fb purple accent-2"
    },
    {
      "color": "#d500f9 purple accent-3"
    },
    {
      "color": "#aa00ff purple accent-4"
    },
    {
      "color": "#ede7f6 deep-purple lighten-5"
    },
    {
      "color": "#d1c4e9 deep-purple lighten-4"
    },
    {
      "color": "#b39ddb deep-purple lighten-3"
    },
    {
      "color": "#9575cd deep-purple lighten-2"
    },
    {
      "color": "#7e57c2 deep-purple lighten-1"
    },
    {
      "color": "#673ab7 deep-purple"
    },
    {
      "color": "#5e35b1 deep-purple darken-1"
    },
    {
      "color": "#512da8 deep-purple darken-2"
    },
    {
      "color": "#4527a0 deep-purple darken-3"
    },
    {
      "color": "#311b92 deep-purple darken-4"
    },
    {
      "color": "#b388ff deep-purple accent-1"
    },
    {
      "color": "#7c4dff deep-purple accent-2"
    },
    {
      "color": "#651fff deep-purple accent-3"
    },
    {
      "color": "#6200ea deep-purple accent-4"
    },
    {
      "color": "#e8eaf6 indigo lighten-5"
    },
    {
      "color": "#c5cae9 indigo lighten-4"
    },
    {
      "color": "#9fa8da indigo lighten-3"
    },
    {
      "color": "#7986cb indigo lighten-2"
    },
    {
      "color": "#5c6bc0 indigo lighten-1"
    },
    {
      "color": "#3f51b5 indigo"
    },
    {
      "color": "#3949ab indigo darken-1"
    },
    {
      "color": "#303f9f indigo darken-2"
    },
    {
      "color": "#283593 indigo darken-3"
    },
    {
      "color": "#1a237e indigo darken-4"
    },
    {
      "color": "#8c9eff indigo accent-1"
    },
    {
      "color": "#536dfe indigo accent-2"
    },
    {
      "color": "#3d5afe indigo accent-3"
    },
    {
      "color": "#304ffe indigo accent-4"
    },
    {
      "color": "#e3f2fd blue lighten-5"
    },
    {
      "color": "#bbdefb blue lighten-4"
    },
    {
      "color": "#90caf9 blue lighten-3"
    },
    {
      "color": "#64b5f6 blue lighten-2"
    },
    {
      "color": "#42a5f5 blue lighten-1"
    },
    {
      "color": "#2196f3 blue"
    },
    {
      "color": "#1e88e5 blue darken-1"
    },
    {
      "color": "#1976d2 blue darken-2"
    },
    {
      "color": "#1565c0 blue darken-3"
    },
    {
      "color": "#0d47a1 blue darken-4"
    },
    {
      "color": "#82b1ff blue accent-1"
    },
    {
      "color": "#448aff blue accent-2"
    },
    {
      "color": "#2979ff blue accent-3"
    },
    {
      "color": "#2962ff blue accent-4"
    },
    {
      "color": "#e1f5fe light-blue lighten-5"
    },
    {
      "color": "#b3e5fc light-blue lighten-4"
    },
    {
      "color": "#81d4fa light-blue lighten-3"
    },
    {
      "color": "#4fc3f7 light-blue lighten-2"
    },
    {
      "color": "#29b6f6 light-blue lighten-1"
    },
    {
      "color": "#03a9f4 light-blue"
    },
    {
      "color": "#039be5 light-blue darken-1"
    },
    {
      "color": "#0288d1 light-blue darken-2"
    },
    {
      "color": "#0277bd light-blue darken-3"
    },
    {
      "color": "#01579b light-blue darken-4"
    },
    {
      "color": "#80d8ff light-blue accent-1"
    },
    {
      "color": "#40c4ff light-blue accent-2"
    },
    {
      "color": "#00b0ff light-blue accent-3"
    },
    {
      "color": "#0091ea light-blue accent-4"
    },
    {
      "color": "#e0f7fa cyan lighten-5"
    },
    {
      "color": "#b2ebf2 cyan lighten-4"
    },
    {
      "color": "#80deea cyan lighten-3"
    },
    {
      "color": "#4dd0e1 cyan lighten-2"
    },
    {
      "color": "#26c6da cyan lighten-1"
    },
    {
      "color": "#00bcd4 cyan"
    },
    {
      "color": "#00acc1 cyan darken-1"
    },
    {
      "color": "#0097a7 cyan darken-2"
    },
    {
      "color": "#00838f cyan darken-3"
    },
    {
      "color": "#006064 cyan darken-4"
    },
    {
      "color": "#84ffff cyan accent-1"
    },
    {
      "color": "#18ffff cyan accent-2"
    },
    {
      "color": "#00e5ff cyan accent-3"
    },
    {
      "color": "#00b8d4 cyan accent-4"
    },
    {
      "color": "#e0f2f1 teal lighten-5"
    },
    {
      "color": "#b2dfdb teal lighten-4"
    },
    {
      "color": "#80cbc4 teal lighten-3"
    },
    {
      "color": "#4db6ac teal lighten-2"
    },
    {
      "color": "#26a69a teal lighten-1"
    },
    {
      "color": "#009688 teal"
    },
    {
      "color": "#00897b teal darken-1"
    },
    {
      "color": "#00796b teal darken-2"
    },
    {
      "color": "#00695c teal darken-3"
    },
    {
      "color": "#004d40 teal darken-4"
    },
    {
      "color": "#a7ffeb teal accent-1"
    },
    {
      "color": "#64ffda teal accent-2"
    },
    {
      "color": "#1de9b6 teal accent-3"
    },
    {
      "color": "#00bfa5 teal accent-4"
    },
    {
      "color": "#e8f5e9 green lighten-5"
    },
    {
      "color": "#c8e6c9 green lighten-4"
    },
    {
      "color": "#a5d6a7 green lighten-3"
    },
    {
      "color": "#81c784 green lighten-2"
    },
    {
      "color": "#66bb6a green lighten-1"
    },
    {
      "color": "#4caf50 green"
    },
    {
      "color": "#43a047 green darken-1"
    },
    {
      "color": "#388e3c green darken-2"
    },
    {
      "color": "#2e7d32 green darken-3"
    },
    {
      "color": "#1b5e20 green darken-4"
    },
    {
      "color": "#b9f6ca green accent-1"
    },
    {
      "color": "#69f0ae green accent-2"
    },
    {
      "color": "#00e676 green accent-3"
    },
    {
      "color": "#00c853 green accent-4"
    },
    {
      "color": "#f1f8e9 light-green lighten-5"
    },
    {
      "color": "#dcedc8 light-green lighten-4"
    },
    {
      "color": "#c5e1a5 light-green lighten-3"
    },
    {
      "color": "#aed581 light-green lighten-2"
    },
    {
      "color": "#9ccc65 light-green lighten-1"
    },
    {
      "color": "#8bc34a light-green"
    },
    {
      "color": "#7cb342 light-green darken-1"
    },
    {
      "color": "#689f38 light-green darken-2"
    },
    {
      "color": "#558b2f light-green darken-3"
    },
    {
      "color": "#33691e light-green darken-4"
    },
    {
      "color": "#ccff90 light-green accent-1"
    },
    {
      "color": "#b2ff59 light-green accent-2"
    },
    {
      "color": "#76ff03 light-green accent-3"
    },
    {
      "color": "#64dd17 light-green accent-4"
    },
    {
      "color": "#f9fbe7 lime lighten-5"
    },
    {
      "color": "#f0f4c3 lime lighten-4"
    },
    {
      "color": "#e6ee9c lime lighten-3"
    },
    {
      "color": "#dce775 lime lighten-2"
    },
    {
      "color": "#d4e157 lime lighten-1"
    },
    {
      "color": "#cddc39 lime"
    },
    {
      "color": "#c0ca33 lime darken-1"
    },
    {
      "color": "#afb42b lime darken-2"
    },
    {
      "color": "#9e9d24 lime darken-3"
    },
    {
      "color": "#827717 lime darken-4"
    },
    {
      "color": "#f4ff81 lime accent-1"
    },
    {
      "color": "#eeff41 lime accent-2"
    },
    {
      "color": "#c6ff00 lime accent-3"
    },
    {
      "color": "#aeea00 lime accent-4"
    },
    {
      "color": "#fffde7 yellow lighten-5"
    },
    {
      "color": "#fff9c4 yellow lighten-4"
    },
    {
      "color": "#fff59d yellow lighten-3"
    },
    {
      "color": "#fff176 yellow lighten-2"
    },
    {
      "color": "#ffee58 yellow lighten-1"
    },
    {
      "color": "#ffeb3b yellow"
    },
    {
      "color": "#fdd835 yellow darken-1"
    },
    {
      "color": "#fbc02d yellow darken-2"
    },
    {
      "color": "#f9a825 yellow darken-3"
    },
    {
      "color": "#f57f17 yellow darken-4"
    },
    {
      "color": "#ffff8d yellow accent-1"
    },
    {
      "color": "#ffff00 yellow accent-2"
    },
    {
      "color": "#ffea00 yellow accent-3"
    },
    {
      "color": "#ffd600 yellow accent-4"
    },
    {
      "color": "#fff8e1 amber lighten-5"
    },
    {
      "color": "#ffecb3 amber lighten-4"
    },
    {
      "color": "#ffe082 amber lighten-3"
    },
    {
      "color": "#ffd54f amber lighten-2"
    },
    {
      "color": "#ffca28 amber lighten-1"
    },
    {
      "color": "#ffc107 amber"
    },
    {
      "color": "#ffb300 amber darken-1"
    },
    {
      "color": "#ffa000 amber darken-2"
    },
    {
      "color": "#ff8f00 amber darken-3"
    },
    {
      "color": "#ff6f00 amber darken-4"
    },
    {
      "color": "#ffe57f amber accent-1"
    },
    {
      "color": "#ffd740 amber accent-2"
    },
    {
      "color": "#ffc400 amber accent-3"
    },
    {
      "color": "#ffab00 amber accent-4"
    },
    {
      "color": "#fff3e0 orange lighten-5"
    },
    {
      "color": "#ffe0b2 orange lighten-4"
    },
    {
      "color": "#ffcc80 orange lighten-3"
    },
    {
      "color": "#ffb74d orange lighten-2"
    },
    {
      "color": "#ffa726 orange lighten-1"
    },
    {
      "color": "#ff9800 orange"
    },
    {
      "color": "#fb8c00 orange darken-1"
    },
    {
      "color": "#f57c00 orange darken-2"
    },
    {
      "color": "#ef6c00 orange darken-3"
    },
    {
      "color": "#e65100 orange darken-4"
    },
    {
      "color": "#ffd180 orange accent-1"
    },
    {
      "color": "#ffab40 orange accent-2"
    },
    {
      "color": "#ff9100 orange accent-3"
    },
    {
      "color": "#ff6d00 orange accent-4"
    },
    {
      "color": "#fbe9e7 deep-orange lighten-5"
    },
    {
      "color": "#ffccbc deep-orange lighten-4"
    },
    {
      "color": "#ffab91 deep-orange lighten-3"
    },
    {
      "color": "#ff8a65 deep-orange lighten-2"
    },
    {
      "color": "#ff7043 deep-orange lighten-1"
    },
    {
      "color": "#ff5722 deep-orange"
    },
    {
      "color": "#f4511e deep-orange darken-1"
    },
    {
      "color": "#e64a19 deep-orange darken-2"
    },
    {
      "color": "#d84315 deep-orange darken-3"
    },
    {
      "color": "#bf360c deep-orange darken-4"
    },
    {
      "color": "#ff9e80 deep-orange accent-1"
    },
    {
      "color": "#ff6e40 deep-orange accent-2"
    },
    {
      "color": "#ff3d00 deep-orange accent-3"
    },
    {
      "color": "#dd2c00 deep-orange accent-4"
    },
    {
      "color": "#efebe9 brown lighten-5"
    },
    {
      "color": "#d7ccc8 brown lighten-4"
    },
    {
      "color": "#bcaaa4 brown lighten-3"
    },
    {
      "color": "#a1887f brown lighten-2"
    },
    {
      "color": "#8d6e63 brown lighten-1"
    },
    {
      "color": "#795548 brown"
    },
    {
      "color": "#6d4c41 brown darken-1"
    },
    {
      "color": "#5d4037 brown darken-2"
    },
    {
      "color": "#4e342e brown darken-3"
    },
    {
      "color": "#3e2723 brown darken-4"
    },
    {
      "color": "#fafafa grey lighten-5"
    },
    {
      "color": "#f5f5f5 grey lighten-4"
    },
    {
      "color": "#eeeeee grey lighten-3"
    },
    {
      "color": "#e0e0e0 grey lighten-2"
    },
    {
      "color": "#bdbdbd grey lighten-1"
    },
    {
      "color": "#9e9e9e grey"
    },
    {
      "color": "#757575 grey darken-1"
    },
    {
      "color": "#616161 grey darken-2"
    },
    {
      "color": "#424242 grey darken-3"
    },
    {
      "color": "#212121 grey darken-4"
    },
    {
      "color": "#eceff1 blue-grey lighten-5"
    },
    {
      "color": "#cfd8dc blue-grey lighten-4"
    },
    {
      "color": "#b0bec5 blue-grey lighten-3"
    },
    {
      "color": "#90a4ae blue-grey lighten-2"
    },
    {
      "color": "#78909c blue-grey lighten-1"
    },
    {
      "color": "#607d8b blue-grey"
    },
    {
      "color": "#546e7a blue-grey darken-1"
    },
    {
      "color": "#455a64 blue-grey darken-2"
    },
    {
      "color": "#37474f blue-grey darken-3"
    },
    {
      "color": "#263238 blue-grey darken-4"
    },
    {
      "color": "#000000 black"
    },
    {
      "color": "#ffffff white"
    },
    {
      "color": "N/A transparent"
    }
  ]
}
';








	$thing= [];
	$json=json_decode($colors);
	foreach($json->collection1 as $color){
		$color = explode(' ', $color->color);
		if(count($color) == 3) {
			list( $hex, $name, $sub ) = $color;

			if(empty($thing[$name]))
				$thing[$name] =[];
			$thing[$name][] = [str_replace('-', '_', $sub) => $hex];
		} else {
			list( $hex, $name )= $color;
			if(empty($thing[$name]))
				$thing[$name] = [];
			$thing[$name][] = $hex;
		}
	}
	return response()->json($thing);
}]);