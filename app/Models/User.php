<?php

namespace App\Models;

use App\Models\Roles\Permission;
use Esensi\Model\Contracts\HashingModelInterface;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\HashingModelTrait;
use Esensi\Model\Traits\ValidatingModelTrait;
use Laravel\Cashier\Billable;
use Laravel\Spark\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatable;
use Laravel\Spark\Contracts\Auth\TwoFactor\Authenticatable as TwoFactorAuthenticatableContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Kregel\Dispatch\Models\Ticket;
use Kregel\Dispatch\Models\TicketUser;
class User extends BaseUser implements TwoFactorAuthenticatableContract, HashingModelInterface
{
    use Billable, TwoFactorAuthenticatable, EntrustUserTrait, HashingModelTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'using_two_factor_auth'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'card_brand',
        'card_last_four',
        'extra_billing_info',
        'password',
        'remember_token',
        'stripe_id',
        'stripe_subscription',
        'two_factor_options',
    ]; 
    public function perms(){
        return $this->belongsToMany(Permission::class);
    }
    
    
    public function closed_tickets(){
        return $this->hasMany(config('kregel.dispatch.models.ticket'), 'closer_id');
    }
    
    public function tickets(){
        return $this->hasMany(config('kregel.dispatch.models.ticket'), 'owner_id');
    }

    public function assigned_tickets(){
        return $this->belongsToMany(config('kregel.dispatch.models.ticket'), 'dispatch_ticket_user', 'user_id', 'ticket_id');
    }

    public function jurisdiction(){
        return $this->belongsToMany(config('kregel.dispatch.models.jurisdiction'), 'dispatch_jurisdiction_user', 'user_id', 'jurisdiction_id');
    }

    public function isAdmin(){
        return $this->hasRole('developer') || $this->hasRole('saycomputer-admin') || $this->hasRole('admin');
    }
}

