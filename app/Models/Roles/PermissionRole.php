<?php

namespace App\Models\Roles;

use Eloquent;

class PermissionRole extends Eloquent
{
    protected $table = 'permission_role';

}