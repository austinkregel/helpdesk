<?php namespace App\Models\Roles;

use Acoustep\EntrustGui\Models\Permission as AcustepPerm;
use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole implements ValidatingModelInterface
{
    use ValidatingModelTrait;
    
    protected $table = 'roles';
    
    protected $throwValidationExceptions = true;

    protected $fillable = [
        'name',
        'display_name',
        'description',
    ];

    protected $rules = [
        'name' => 'required|unique:roles',
        'display_name' => 'required|unique:roles',
    ];

    public function permission()
    {
        return $this->belongsToMany(Permission::class, 'permission_role', 'id', 'permission_id');
    }
    public function services(){
        return $this->hasMany(\App\Models\Services::class);
    }
}
