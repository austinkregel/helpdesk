<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
function uuid($data)
{

    assert(strlen($data) == 16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    echo 'Making user ' . asdf::add() . "\n";
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
    ];
});
$factory->define(Kregel\Dispatch\Models\Ticket::class, function (Faker\Generator $faker) {

    echo 'Making ticket ' . asdf::add() . "\n";
    return [
        'uuid' => uuid(openssl_random_pseudo_bytes(16)),
        'title' => $faker->name,
        'body' => $faker->paragraph . $faker->paragraph . $faker->paragraph . $faker->paragraph . $faker->paragraph . $faker->paragraph . $faker->paragraph,
        'finish_by' => $faker->date,
        'owner_id' => mt_rand(1, 100),
        'jurisdiction_id' => 1,
    ];
});
$i = 0;
$factory->define(App\Models\Roles\Permission::class, function (Faker\Generator $faker) use (&$i){
    $prefixes = ['view-', 'edit-', 'create-', 'delete-'];
    $stuff = [];
    foreach(config('kregel.warden.models') as $key => $val)
        $stuff[] = $key;

    $names = array_merge($stuff, [
        'permission',
        'role',
        'message',
        'tags',
        'manager',
        'locations'
    ]);
    $perms = [];

    foreach ($names as $name) {
        foreach ($prefixes as $pre) {
            $perms[] = $pre . $name;
        }
    }
    echo 'Making permission ' . $i . ' at: '.$perms[$i] ." $i\n";
    
    return [
        'name' => str_slug($perms[$i++]),
    ];
});
$j=0;
$factory->define(App\Models\Roles\Role::class, function (Faker\Generator $faker) use (&$j) {

    echo 'Making role ' . $j . "\n";
    $roles = [
        'developer', 
        'saycomputer-admin',
        'admin',
        'manager',
        'user'
    ];

    return [
        'name' => str_slug($roles[$j++]),
        'display_name' => $roles[$j]
    ];
});
$o = 0;
$factory->define(Kregel\Dispatch\Models\Jurisdiction::class, function (Faker\Generator $faker) use (&$o){
    echo 'Making Jurisdiction' . $o . "\n";

    return [
        'name' => $faker->company,
        'phone_number' => $faker->phoneNumber
    ];
});

class asdf
{
    public static $instance;
    public $i = 0;

    public static function __callStatic($method, $args)
    {
        if (empty(static::$instance)) {
            static::$instance = new static;
        }
        $inst = static::$instance;
        return $inst->$method();
    }

    private function add()
    {
        return $this->i = $this->i + 1;
    }

    private function getI()
    {
        return $this->i;
    }

    private function reset()
    {
        $this->i = 0;
    }
}


