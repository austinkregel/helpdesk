<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Roles\Permission;
use App\Models\Roles\Role;
use App\Models\Roles\RolePermission;
use App\Models\Roles\RolesRelated;
use App\Models\Roles\RoleUser;
use App\Models\User;
class Corrections extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return String
     */

    function uuid($data)
    {

        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    public function run()
    {

        //User::create([
        //    'name' => 'Austin Kregel',
        //    'email' => 'austinkregel@gmail.com',
        //    'password' => bcrypt('000000')
        //]);
        factory(User::class, 50)->create();
        factory(Role::class, 4)->create();
        factory(Permission::class, 35)->create();
        echo 'Finished with factories'."\n";
        echo 'Moving on to Roles/Permissions'."\n";
        $perms = Permission::all();

        $roles = Role::all();


        foreach($roles as $role) {
            $i = 0;
            for($j = 0;$j < round($perms->count()/$role->id); $j++) {
                $perm = $perms[$j];
                $role->attachPermission($perm);
            }

        }
        echo 'Finished with Role/Permissions'."\n";
        echo 'Moving on to Role/Users'."\n";
        $users = User::where('id', 'not', 1)->get();
        $role_max = Role::all()->count();

        foreach($users as $user){
            echo 'user->roles()->assign'. "\n";
            $user->attachRole(Role::find(mt_rand(1, $role_max)));
        }

		$me = User::whereEmail('austinkregel@gmail.com')->first();
		$role = Role::whereName('developer')->first();
		$me->roles()->attach($role->id);
		factory(Kregel\Dispatch\Models\Jurisdiction::class, 10)->create();
		foreach(Kregel\Dispatch\Models\Jurisdiction::all() as $jur){
			if(!$me->jurisdiction->contains($jur->id))
            \DB::table('dispatch_jurisdiction_user')->insert([
                'jurisdiction_id' => $jur->id,
                'user_id' => $me->id,
            ]);
		}

		$faker = Faker\Factory::create();
		$jur = Kregel\Dispatch\Models\Jurisdiction::all();
		//dd($faker);

		\Kregel\Dispatch\Models\Priority::create([
			'name' => 'A',
			'deadline' => '+1 week'
		]);
		\Kregel\Dispatch\Models\Priority::create([
			'name' => 'B',
			'deadline' => '+2 weeks'
		]);
		\Kregel\Dispatch\Models\Priority::create([
			'name' => 'C',
			'deadline' => '+1 month'
		]);
		\Kregel\Dispatch\Models\Priority::create([
			'name' => 'D',
			'deadline' => '+6 weeks'
		]);
		$priority = \Kregel\Dispatch\Models\Priority::all();
		for($i=0; $i< 100; $i++)
			Kregel\Dispatch\Models\Ticket::create([
				'title' => $faker->realText(30),
				'body' => $faker->paragraph . $faker->paragraph . $faker->paragraph . $faker->paragraph . $faker->paragraph . $faker->paragraph . $faker->paragraph,
				'priority_id' => $priority->find(mt_rand(1, $priority->count()))->id,
				'owner_id' => $me->id,
				'jurisdiction_id' => $jur->find(mt_rand(1, $jur->count()))->id,
			]);

	}
}
